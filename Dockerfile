FROM centos
LABEL v1=1.0
MAINTAINER TG
RUN yum install httpd -y
WORKDIR /var/www/html
ENTRYPOINT ["httpd", "-DFOREGROUND"]
EXPOSE 80
